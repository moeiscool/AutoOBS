const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
const OBSController = require('./libs/obsControl');

const app = express();
const server = http.createServer(app);
const io = socketIo(server);

// Initialize OBSController
const obsController = new OBSController('ws://localhost:4455', 'MItIiMyvt1Xnj5zb');

app.use(express.static('public'));

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/public/index.html');
});

let isStreaming = false;
obsController.obs.on('StreamStateChanged', (data) => {
    const isStreaming = data.outputActive;
    io.emit('streamingStatus', isStreaming);
});

io.on('connection', (socket) => {
    // console.log('A user connected');
    socket.emit('streamingStatus', isStreaming);

    socket.on('startStreaming', async () => {
        try {
            await obsController.startStreaming();
            isStreaming = true;
            io.emit('streamingStatus', isStreaming);
        } catch (error) {
            // console.error('Error starting streaming:', error);
            socket.emit('error', 'Failed to start streaming');
        }
    });

    socket.on('stopStreaming', async () => {
        try {
            await obsController.stopStreaming();
            isStreaming = false;
            io.emit('streamingStatus', isStreaming);
        } catch (error) {
            // console.error('Error stopping streaming:', error);
            socket.emit('error', 'Failed to stop streaming');
        }
    });
});

const PORT = process.env.PORT || 3000;
server.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
