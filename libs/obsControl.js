const exec = require('child_process').exec;
const OBSWebSocket = require('obs-websocket-js').default;
function waitTime(time){
    return new Promise((resolve) => {
        setTimeout(resolve,time)
    })
}
class OBSController {
    constructor(url, password) {
        this.obs = new OBSWebSocket();
        this.url = url;
        this.password = password;
    }

    async getSourceScreenshot(sourceName, imageFormat = 'png', imageCompressionQuality = 100, imageWidth = 0, imageHeight = 0) {
        if(!this.connected)await this.connect();
        try {
            const data = {
                requestType: 'GetSourceScreenshot',
                requestData: {
                    sourceName,
                    imageFormat,
                    imageCompressionQuality,
                    imageWidth,
                    imageHeight
                }
            };
            const response = await this.obs.call(data.requestType, data.requestData);
            return response;
        } catch (error) {
            console.error('Error taking screenshot:', error);
            throw error;
        }
    }

    async connect() {
        try {
            const { obsWebSocketVersion, negotiatedRpcVersion } = await this.obs.connect(this.url, this.password);
            // console.log(`Connected to server ${obsWebSocketVersion} (using RPC ${negotiatedRpcVersion})`);
        } catch (error) {
            console.error('Failed to connect OBS, Retrying...', new Date());
            await waitTime(2000)
            await this.connect()
        }
    }

    launchOBS() {
        return new Promise((resolve) => {
            console.error('Launching OBS...');
            const pathToOBS = `${process.cwd()}/startOBS.bat`; // Update to your OBS installation path
            exec(pathToOBS, (err) => {
                if (err) {
                    console.error('Failed to launch OBS:', err);
                    return;
                }
                console.log('OBS launched successfully.');
                resolve()
            });
        })
    }

    stopOBS() {
        // console.error('Launching OBS...');
        const pathToOBS = `${process.cwd()}/stopOBS.bat`; // Update to your OBS installation path
        exec(pathToOBS, (err) => {
            if (err) {
                console.error('Failed to launch OBS:', err);
                return;
            }
            // console.log('OBS launched successfully.');
        });
    }

    async startStreaming() {

        await this.launchOBS();
        await waitTime(2000)
        await this.connect();

        try {
            await this.obs.call('StartStream');
            // console.log('Streaming started.');
        } catch (error) {
            console.error('Error starting streaming:', error);
            throw error;
        }
    }

    async stopStreaming() {
        if(!this.connected)await this.connect();

        try {
            await this.obs.call('StopStream');
            // console.log('Streaming stopped.');
            await this.stopOBS();
        } catch (error) {
            console.error('Error stopping streaming:', error);
            throw error;
        }
    }
}

module.exports = OBSController;
