const socket = io();
const status = document.getElementById('status');
socket.on('streamingStatus', (isStreaming) => {
    console.log('Streaming status:', isStreaming);
    status.innerHTML = `<div style="width:50px;height:50px;border-radius:50%;background-color: ${!!isStreaming ? 'green' : 'red'}"></div>`
    // Update UI based on streaming status
});

document.getElementById('startBtn').addEventListener('click', () => {
    socket.emit('startStreaming');
});

document.getElementById('stopBtn').addEventListener('click', () => {
    socket.emit('stopStreaming');
});
